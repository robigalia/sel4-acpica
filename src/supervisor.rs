
// Supervise resource requests made by the ACPICA thread.

use acpica_sys::*;

#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
    Map { paddr: usize, len: usize },
    Unmap { laddr: usize, len: usize },
    GetPAddr { laddr: usize },
    InstallInterruptHandler { irq: u32 },
    RemoveInterruptHandler { irq: u32 },
    ReadPort { ioaddr: u32, width: u8 },
    WritePort { ioaddr: u32, val: U32, width: u8 },
    ReadMemory { paddr: usize, width: u8 },
    WriteMemory { paddr: usize, val: u64, width: u8 },
    ReadPCI { pciid: pci::Location, reg: u32, width: u8 },
    WritePCI { pciid: pci::Location, reg: u32, val: u64, width: u8 },
    Readable { laddr: u64, len: u64 },
    Writable { laddr: u64, len: u64 },
    GetRootPointer,
    Sleep { ms: u64 },
    MoreMemory { size: usize },
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Response {
    Map { laddr: usize },
    GetPAddr { paddr: usize },
    ReadPort { val: u32 },
    ReadMemory { val: u64 },
    ReadPCI { val: u64 },
    GetTimer { val: u64 },
    Readable { val: bool },
    Writable { val: bool },
    GetRootPointer { val: usize },
    MoreMemory { addr: usize },
    NoResponseNeeded,
}


#[derive(Serialize, Deserialize, Debug)]
pub enum Error {
    NotAllowed,
    NotFound,
    NotExist,
    NoMemory,
    AllreadyExists,
}
