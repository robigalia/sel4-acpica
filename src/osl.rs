#![allow(bad_style, dead_code, unused_variables)]

// "Operating System Services Layer".

use acpica_sys::*;
use core::mem::{size_of, align_of};
use core::ptr::{null_mut};
use sel4;
use sel4_sys::{seL4_GetIPCBuffer, seL4_Word};
use core_io::Cursor;
use byteorder::{ReadBytesExt, WriteBytesExt, NativeEndian};

use supervisor::Request;

pub const PARENT_EP: sel4::Endpoint = sel4::Endpoint::from_cap(1);
pub const DELAY_PORT: sel4::IOPort = sel4::IOPort::from_cap(2);

unsafe fn alloc_typed<T>() -> *mut T {
    ::alloc::heap::allocate(size_of::<T>(), align_of::<T>()) as *mut T
}

unsafe fn free_typed<T>(x: *mut T) {
    ::alloc::heap::deallocate(x as *mut u8, size_of::<T>(), align_of::<T>());
}

/// Safety note: be careful with aliasing.
unsafe fn ipc_buf_mut<'a>() -> &'static mut [u8] {
    let buf = seL4_GetIPCBuffer();
    ::core::slice::from_raw_parts_mut((&mut (*buf).msg).as_ptr() as *mut u8, (*buf).msg.len())
}

/// Safety note: be careful with aliasing.
unsafe fn ipc_buf<'a>() -> &'static [u8] {
    let buf = seL4_GetIPCBuffer();
    ::core::slice::from_raw_parts(&(*buf).msg as *const u32 as *const u8, (&(*buf).msg).len())
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsInitialize() -> ACPI_STATUS {
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsTerminate() -> ACPI_STATUS {
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetRootPointer() -> ACPI_PHYSICAL_ADDRESS {
    match supervise(GetRootPointer) {
        Ok(GetRootPointer { val }) => {
            val
        },
        Err(_) | Ok(_) => panic!("could not get root pointer!")
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsPredefinedOverride(InitVal: *const ACPI_PREDEFINED_NAMES,
                                       NewVal: *mut ACPI_STRING) -> ACPI_STATUS {
    if NewVal.is_null() {
        return AE_BAD_PARAMETER
    }
    unsafe {
        *NewVal = null_mut();
    }
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsTableOverride(ExistingTable: *mut ACPI_TABLE_HEADER,
                                  NewTable: *mut *mut ACPI_TABLE_HEADER) 
-> ACPI_STATUS {
    if NewTable.is_null() {
        return AE_BAD_PARAMETER
    }
    unsafe {
        *NewTable = null_mut();
    }
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsPhysicalTableOverride(ExistingTable: *mut ACPI_TABLE_HEADER,
                                          NewAddress: *mut ACPI_PHYSICAL_ADDRESS,
                                          NewTableLength: *mut UINT32)
-> ACPI_STATUS {
    if NewAddress.is_null() {
        return AE_BAD_PARAMETER
    }
    unsafe {
        *NewAddress = 0;
    }
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsCreateLock(OutHandle: *mut *mut c_void) -> ACPI_STATUS {
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsDeleteLock(Handle: *mut c_void) {
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsAcquireLock(Handle: *mut c_void)
-> ACPI_SIZE {
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsReleaseLock(Handle: *mut c_void,
                                Flags: ACPI_SIZE) {
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsCreateSemaphore(MaxUnits: UINT32, InitialUnits: UINT32,
                                    OutHandle: *mut *mut c_void)
-> ACPI_STATUS {
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsDeleteSemaphore(Handle: *mut c_void)
-> ACPI_STATUS {
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsWaitSemaphore(Handle: *mut c_void,
                                  Units: UINT32, Timeout: UINT16) -> ACPI_STATUS {
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsSignalSemaphore(Handle: *mut c_void,
                                    Units: UINT32) -> ACPI_STATUS {
    AE_OK
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsAllocate(Size: ACPI_SIZE) -> *mut u8 {
    let ptr = unsafe {
        ::alloc::heap::allocate((Size + (size_of::<ACPI_SIZE>() as ACPI_SIZE)) as usize, 1)
    };
    if ptr.is_null() {
        return ptr;
    }
    unsafe {
        *(ptr as *mut ACPI_SIZE) = Size;
        ptr.offset(size_of::<ACPI_SIZE>() as isize)
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsAllocateZeroed(Size: ACPI_SIZE) -> *mut u8 {
    let ptr = AcpiOsAllocate(Size);
    if ptr.is_null() {
        return ptr;
    }
    unsafe {
        ::core::ptr::write_bytes(ptr, 0, Size as usize);
    }
    ptr
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsFree(Memory: *mut u8) {
    unsafe {
        let real_ptr = Memory.offset(-(size_of::<ACPI_SIZE>() as isize));
        let real_size = *(real_ptr as *mut ACPI_SIZE);
        ::alloc::heap::deallocate(Memory as *mut u8, real_size as usize + size_of::<ACPI_SIZE>(), 1);
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsMapMemory(Where: ACPI_PHYSICAL_ADDRESS, Length: ACPI_SIZE) -> *mut c_void {
    match supervise(Map { paddr: Where as usize, len: Length as usize }) {
        Ok(Map { laddr }) => {
            laddr as *mut c_void
        }
        Ok(_) | Err(_) => panic!("map failed")
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsUnmapMemory(LogicalAddress: *mut c_void,
                                Size: ACPI_SIZE) {
    match supervise(Unmap { laddr: LogicalAddress as usize, len: Size as usize }) {
        Ok(NoResponseNeeded) => (),
        _ => panic!("unmap failed")
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetPhysicalAddress(LogicalAddress:
                                       *mut c_void,
                                       PhysicalAddress:
                                       *mut ACPI_PHYSICAL_ADDRESS)
-> ACPI_STATUS {
    if LogicalAddress.is_null() || PhysicalAddress.is_null() {
        return AE_BAD_PARAMETER;
    }

    match supervise(GetPAddr { laddr: LogicalAddress as usize }) {
        Ok(GetPAddr { paddr }) => {
            unsafe {
                *PhysicalAddress = paddr as *mut _;
            }
            AE_OK
        },
        _ => AE_ERROR
    }
}

// These shouldn't be needed, ACPICA is compiled with its internal caching mechanism

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsCreateCache(CacheName: *mut i8,
                                ObjectSize: UINT16, MaxDepth: UINT16,
                                ReturnCache: *mut *mut ACPI_MEMORY_LIST)
-> ACPI_STATUS {
    // Intentionally unsupported - rely on acpica's internal cache
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsDeleteCache(Cache: *mut ACPI_MEMORY_LIST) -> ACPI_STATUS {
    // Intentionally unsupported - rely on acpica's internal cache
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsPurgeCache(Cache: *mut ACPI_MEMORY_LIST) -> ACPI_STATUS {
    // Intentionally unsupported - rely on acpica's internal cache
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsAcquireObject(Cache: *mut ACPI_MEMORY_LIST)
-> *mut c_void {
    // Intentionally unsupported - rely on acpica's internal cache
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsReleaseObject(Cache: *mut ACPI_MEMORY_LIST,
                                  Object: *mut c_void)
-> ACPI_STATUS {
    // Intentionally unsupported - rely on acpica's internal cache
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsInstallInterruptHandler(InterruptNumber: UINT32,
                                            ServiceRoutine: ACPI_OSD_HANDLER,
                                            Context: *mut c_void)
-> ACPI_STATUS {
    match ServiceRoutine {
        Some(cb) => {
            match supervise(InstallInterruptHandler { irq: InterruptNumber }) {
                Ok(NoResponseNeeded) => {
                    match install_handler(cb, Context) {
                        Ok(_) => AE_OK,
                        Err(_) => AE_ALREADY_EXISTS,
                    }
                },
                _ => AE_BAD_PARAMETER
            }
        },
        _ => AE_BAD_PARAMETER
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsRemoveInterruptHandler(InterruptNumber: UINT32,
                                               ServiceRoutine: ACPI_OSD_HANDLER)
-> ACPI_STATUS {
    match ServiceRoutine {
        Some(cb) => {
            match handler_remove(cb, InterruptNumber) {
                HandlerStatus::NotInstalled => return AE_NOT_EXIST,
                HandlerStatus::WrongCallback => return AE_BAD_PARAMETER,
                HandlerStatus::Matches => match supervise(RemoveInterruptHandler { irq: InterruptNumber }) {
                    Ok(NoResponseNeeded) => {
                        AE_OK
                    },
                    _ => AE_BAD_PARAMETER
                }
            }
        },
        _ => AE_BAD_PARAMETER
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetThreadId() -> UINT64 {
    // Only one thread for now.
    0
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsExecute(Type: ACPI_EXECUTE_TYPE,
                                Function: ACPI_OSD_EXEC_CALLBACK,
                                Context: *mut c_void) -> ACPI_STATUS {
    match Function {
        Some(f) => {
            unsafe {
                f(Context);
            }
            AE_OK
        },
        None => AE_BAD_PARAMETER
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsWaitEventsComplete() {
    // Since we don't do async OsExecute, no need to wait.
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsSleep(Milliseconds: UINT64) {
    let _ = supervise(Sleep { ms: Milliseconds });
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsStall(Microseconds: UINT32) {
    for _ in 0..Microseconds {
        // Apparently, this takes about 1us. Plus some fudge for the syscall.
        // maybe FIXME: workaround for https://lwn.net/Articles/263418/ ?
        DELAY_PORT.write8(0x80, 0).expect("DELAY_PORT not setup");
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsReadPort(Address: ACPI_IO_ADDRESS, Value: *mut UINT32,
                                 Width: UINT32) -> ACPI_STATUS {
    match supervise(ReadPort { ioaddr: Address, width: Width as u8 }) {
        Ok(ReadPort { val }) => {
            unsafe {
                *Value = val;
            }
            AE_OK
        },
        _ => AE_ERROR
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsWritePort(Address: ACPI_IO_ADDRESS, Value: UINT32,
                                  Width: UINT32) -> ACPI_STATUS {
    match supervise(WritePort { ioaddr: Address, val: Value, width: Width as u8 }) {
        Ok(NoResponseNeeded) => AE_OK,
        _ => AE_ERROR
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsReadMemory(Address: ACPI_PHYSICAL_ADDRESS,
                                   Value: *mut UINT64, Width: UINT32) -> ACPI_STATUS {
    match supervise(ReadMemory { paddr: Address, width: Width as u8 }) {
        Ok(ReadMemory { val }) => {
            unsafe {
                *Value = val;
            }
            AE_OK
        },
        _ => AE_ERROR
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsWriteMemory(Address: ACPI_PHYSICAL_ADDRESS, Value: UINT64,
                                    Width: UINT32) -> ACPI_STATUS {
    match supervise(WriteMemory { paddr: Address, val: Value, width: Width as u8 }) {
        Ok(NoResponseNeeded) => AE_OK,
        _ => AE_ERROR
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsReadPciConfiguration(PciId: *mut ACPI_PCI_ID, Reg: UINT32,
                                             Value: *mut UINT64, Width: UINT32)
-> ACPI_STATUS {
    match to_pciloc(PciId) {
        Some(pciid) => {
            match supervise(ReadPCI { pciid: pciid, reg: Reg, width: Width as u8 }) {
                Ok(ReadPCI { val }) => {
                    unsafe {
                        *Value = val;
                    }
                    AE_OK
                },
                _ => AE_ERROR
            }
        },
        None => AE_ERROR
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsWritePciConfiguration(PciId: *mut ACPI_PCI_ID, Reg: UINT32,
                                              Value: UINT64, Width: UINT32)
-> ACPI_STATUS {
    match to_pciloc(PciId) {
        Some(pciid) => {
            match supervise(ReadPCI { pciid: pciid, reg: Reg, width: Width as u8 }) {
                Ok(ReadPCI { val }) => {
                    unsafe {
                        *Value = val;
                    }
                    AE_OK
                },
                _ => AE_ERROR
            }
        },
        None => AE_ERROR
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsReadable(Pointer: *mut c_void,
                                 Length: ACPI_SIZE) -> BOOLEAN {
    match supervise(Readable { laddr: Pointer as usize, len: Length as u64 }) {
        Ok(Readable { val }) => val,
        _ => false
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsWritable(Pointer: *mut c_void,
                                 Length: ACPI_SIZE) -> BOOLEAN {
    match supervise(Writable { laddr: Pointer as usize, len: Length as u64 }) {
        Ok(Writable { val }) => val,
        _ => false
    }
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetTimer() -> UINT64 {
    ::tempo::get_now().to_ticks()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsSignal(Function: UINT32, Info: *mut c_void)
-> ACPI_STATUS {
    // FIXME
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsVprintf(Format: *const i8,
                                Args: va_list) {
    // FIXME
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsRedirectOutput(Destination: *mut c_void) {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetLine(Buffer: *mut i8,
                                BufferLength: UINT32, BytesRead: *mut UINT32)
-> ACPI_STATUS {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetTableByName(Signature: *mut i8,
                                       Instance: UINT32,
                                       Table: *mut *mut ACPI_TABLE_HEADER,
                                       Address: *mut ACPI_PHYSICAL_ADDRESS)
-> ACPI_STATUS {
    AE_SUPPORT
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetTableByIndex(Index: UINT32,
                                        Table: *mut *mut ACPI_TABLE_HEADER,
                                        Instance: *mut UINT32,
                                        Address: *mut ACPI_PHYSICAL_ADDRESS)
-> ACPI_STATUS {
    AE_SUPPORT
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetTableByAddress(Address: ACPI_PHYSICAL_ADDRESS,
                                          Table: *mut *mut ACPI_TABLE_HEADER)
-> ACPI_STATUS {
    AE_SUPPORT
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsOpenDirectory(Pathname: *mut i8,
                                      WildcardSpec: *mut i8,
                                      RequestedFileType: i8)
-> *mut c_void {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetNextFilename(DirHandle: *mut c_void)
-> *mut i8 {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsCloseDirectory(DirHandle: *mut c_void) {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsOpenFile(Path: *const i8, Modes: UINT8)
-> *mut c_void {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsCloseFile(File: *mut c_void) {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsReadFile(File: *mut c_void,
                                 Buffer: *mut c_void,
                                 Size: ACPI_SIZE, Count: ACPI_SIZE)
-> i32 {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsWriteFile(File: *mut c_void,
                                  Buffer: *mut c_void,
                                  Size: ACPI_SIZE, Count: ACPI_SIZE)
-> i32 {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsGetFileOffset(File: *mut c_void)
-> i32 {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsSetFileOffset(File: *mut c_void,
                                      Offset: i32, From: UINT8)
-> ACPI_STATUS {
    // Intentionally unsupported
    unimplemented!()
}

#[no_mangle]
#[linkage="external"]
pub extern "C" fn AcpiOsTracePoint(Type: ACPI_TRACE_EVENT_TYPE, Begin: BOOLEAN,
                                   Aml: *mut UINT8,
                                   Pathname: *mut i8) {
    // Intentionally unsupported
    unimplemented!()
}
