#![feature(linkage, alloc, heap_api, const_fn)]
#![no_std]

#![allow(dead_code)]

extern crate acpica_sys;
extern crate alloc;
extern crate sel4;
extern crate sel4_sys;
extern crate byteorder;
extern crate core_io;

pub mod osl;
mod supervisor;
